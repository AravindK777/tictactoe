﻿using System;

namespace TicTacToe
{
    class Program
    {
        char[,] board;
        int size;
        int rowInput, colInput;
        const char DEFAULTCHAR = ' ';
        char USER = 'X';
        bool boardFilled = false;

        public Program()
        {
            //board = new char[,];
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to TicTacToe");

            Program p = new Program();
            p.GetSize();

            p.PlayGame();

            //p.PrintBoard();
            Console.Read();
        }

        public void GetSize()
        {
            while (true)
            {
                Console.Write("Enter the board size: ");
                var input = Console.ReadLine();
                if (!string.IsNullOrEmpty(input))
                {
                    size = Convert.ToInt32(input);
                    if (size % 2 == 0 || size < 3)
                    {
                        Console.WriteLine("Invalid input. Please try again... ");
                        continue;
                    }
                    else
                    {
                        board = new char[size, size];
                        break;
                    }
                }
            }

            for (int row = 0; row < size; row++)
            {
                for (int column = 0; column < size; column++)
                {
                    board[row, column] = new char();
                    board[row, column] = DEFAULTCHAR;
                }
            }
        }

        public void PrintBoard()
        {
            for (int row = 0; row < size; row++)
            {
                Console.WriteLine();
                for (int column = 0; column < size; column++)
                {
                    if (column == 0) Console.Write(" | ");
                    Console.Write(board[row, column] + " | ");
                }
            }
            Console.WriteLine();
        }

        public void PlayGame()
        {

            bool playing = true;
            //print();
            while (playing)
            {
                Console.Write("Enter your choice as (row, column): ");
                var inputChoice = Console.ReadLine();
                if (!string.IsNullOrEmpty(inputChoice))
                {
                    rowInput = Convert.ToInt32(inputChoice.Split(',')[0]);
                    colInput = Convert.ToInt32(inputChoice.Split(',')[1]);

                    board[rowInput, colInput] = USER;
                    PrintBoard();
                    if (IsEnding(rowInput, colInput))
                    {
                        playing = false;
                        Console.WriteLine("Player " + USER + " wins!");
                    }

                    if (USER == 'X')
                        USER = 'O';
                    else
                        USER = 'X';
                }
            }
        }


        public bool IsEnding(int rowMove, int columnMove)
        {
            bool turnExists = true;
            int row = 0, col = 0;
            //for (int i = 0; i < size; i++)
            //{
            //    for (int j = 0; j < size; j++)
            //    {
            //        if (board[i,columnMove] == board[1,columnMove] && board[0,columnMove] == board[2,columnMove])
            //            return true;

            //        //row victory
            //        if (board[rowMove,0] == board[rowMove,1] && board[rowMove,0] == board[rowMove,2])
            //            return true;

            //        //diagonal victory from top
            //        if (board[0,0] == board[1,1] && board[0,0] == board[2,2] && board[1,1] != DEFAULTCHAR)
            //            return true;

            //        //diagonal victory from bottom 
            //        if (board[0,2] == board[1,1] && board[0,2] == board[2,0] && board[1,1] != DEFAULTCHAR)
            //            return true;

            //        //if board is filled but there are no winners
            //    }
            //}

            //column victory 
            for (int column = 0; column < size; column++)
            {
                if (board[rowMove, column] != USER)
                {
                    turnExists = false; break;
                }
            }
            if (turnExists) return true;

            for (int rows = 0; rows < size; rows++)
            {
                if (board[rows, columnMove] != USER)
                {
                    turnExists = false;
                    break;
                }
            }
            if (turnExists) return true;
            
            // diagonal - left top to right bottom
            while (row < size && col < size)
            {
                if (board[row, col] != USER)
                {
                    turnExists = false;
                    break;
                }
                row = col++;
            }
            if (turnExists) return true;

            // diagonal - right top to left bottom
            row = 0;
            col = size - 1;
            while (row < size && col >=0)
            {
                if (board[row, col] != USER)
                {
                    turnExists = false;
                    break;
                }
                row++;
                col--;
            }

            return turnExists;
        }

    }
}
